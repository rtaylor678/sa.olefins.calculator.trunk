﻿CREATE FUNCTION [dim].[Return_ProcessUnitId]
(
	@ProcessUnitTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[ProcessUnitId]
	FROM [dim].[ProcessUnit_LookUp]	l
	WHERE l.[ProcessUnitTag] = @ProcessUnitTag;

	RETURN @Id;

END;