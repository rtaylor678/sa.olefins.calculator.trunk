﻿CREATE PROCEDURE [web].[Update_StreamDescription]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamDescription		NVARCHAR(256)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[StreamDescription] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber)
	BEGIN
		EXECUTE [stage].[Update_StreamDescription] @SubmissionId, @StreamNumber, @StreamDescription;
	END;
	ELSE
	BEGIN
		IF(@StreamDescription	<> '')
		EXECUTE [stage].[Insert_StreamDescription] @SubmissionId, @StreamNumber, @StreamDescription;
	END;

END;