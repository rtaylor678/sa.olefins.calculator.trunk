﻿CREATE FUNCTION [dim].[Return_StreamId]
(
	@StreamTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[StreamId]
	FROM [dim].[Stream_LookUp]	l
	WHERE l.[StreamTag] = @StreamTag;

	RETURN @Id;

END;