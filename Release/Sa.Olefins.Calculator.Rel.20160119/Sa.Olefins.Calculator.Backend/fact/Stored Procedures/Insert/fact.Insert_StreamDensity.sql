﻿CREATE PROCEDURE [fact].[Insert_StreamDensity]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[StreamDensity]([SubmissionId], [StreamNumber], [Density_SG])
	SELECT
		d.[SubmissionId],
		d.[StreamNumber],
		d.[Density_SG]
	FROM [stage].[StreamDensity]		d
	INNER JOIN [stage].[StreamQuantity]	q
		ON	q.[SubmissionId]	= d.[SubmissionId]
		AND	q.[StreamNumber]	= d.[StreamNumber]
		AND	q.[Quantity_kMT]	> 0
	WHERE	d.[SubmissionId]	= @SubmissionId
		AND	d.[Density_SG]		> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;