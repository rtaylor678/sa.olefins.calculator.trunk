﻿CREATE FUNCTION [dim].[Return_FactorId]
(
	@FactorTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[FactorId]
	FROM [dim].[Factor_LookUp]	l
	WHERE l.[FactorTag] = @FactorTag;

	RETURN @Id;

END;