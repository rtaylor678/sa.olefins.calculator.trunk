﻿CREATE PROCEDURE [web].[Get_PGasHydrotreaterType]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		l.[HydroTreaterTypeId]		[PGasHydroTreaterTypeId],
		l.[HydroTreaterTypeName]	[PGasHydroTreaterTypeDesc]
	FROM [dim].[HydroTreaterType_LookUp]	l
	WHERE l.[HydroTreaterTypeId] >= 2
	ORDER BY
		l.[HydroTreaterTypeName] ASC;

END;