﻿CREATE PROCEDURE [web].[Update_JoinPlantSubmission]
(
	@JoinId					INT	= NULL,
	@PlantId				INT	= NULL,
	@SubmissionId			INT	= NULL,
	@Active					BIT	= NULL	
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM [auth].[JoinPlantSubmission] jps WHERE jps.[SubmissionId] = @SubmissionId AND jps.[PlantId] = @PlantId)
	BEGIN
		EXECUTE @JoinId = [auth].[Update_JoinPlantSubmission] @JoinId, @PlantId, @SubmissionId, @Active;
	END
	ELSE
	BEGIN
		EXECUTE @JoinId = [auth].[Insert_JoinPlantSubmission] @PlantId, @SubmissionId;
	END;

	SELECT @JoinId;
	RETURN @JoinId;

END;