﻿CREATE FUNCTION [dim].[Return_SeverityId]
(
	@SeverityTag		VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[SeverityId]
	FROM [dim].[Severity_LookUp]	l
	WHERE l.[SeverityTag] = @SeverityTag;

	RETURN @Id;

END;